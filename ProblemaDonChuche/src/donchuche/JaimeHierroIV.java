package donchuche;

import java.util.ArrayList;
import java.util.Scanner;

public class JaimeHierroIV {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();
		sc.nextLine();

		for (int i = 0; i < casos; i++) {

			ArrayList<String> nombres = new ArrayList<String>();

			int grupo = sc.nextInt();
			sc.nextLine();

			for (int j = 0; j < grupo; j++) {
				nombres.add(sc.nextLine().toLowerCase());
			}

			String util = hierro4(nombres, grupo);

			System.out.println(util);

		}

	}

	public static String hierro4(ArrayList<String> nombres, int grupo) {

		if (grupo < 5 || grupo > 5) {
			nombres.clear();
			return "Mejor no jugamos partidas normales";
		} else if (nombres.contains("jaume") || nombres.contains("teruel")) {
			nombres.clear();
			return "Perdemos, Jaume ha fedeado como de costumbre";
		} else {
			nombres.clear();
			return "Ganamos?";
		}

	}

}
