package donchuche;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class JaimeHierroIVTest {

	@Test
	public void test1() {
		
		ArrayList<String> nombres = new ArrayList<String>();

		nombres.add("dani");
		nombres.add("jaume");
		nombres.add("marc");
		nombres.add("davilillo");
		nombres.add("motoMongo");
	
		assertEquals("Perdemos, Jaume ha fedeado como de costumbre",JaimeHierroIV.hierro4(nombres, 5));
	
	}
	
	@Test
	public void test2() {
		
		ArrayList<String> nombres = new ArrayList<String>();

		nombres.add("dani");
		nombres.add("ethan");
		nombres.add("ruben");
		nombres.add("oscar");
		nombres.add("motoMongo");
	
		assertEquals("Ganamos?",JaimeHierroIV.hierro4(nombres, 5));
	
	}
	
	@Test
	public void test3() {
		
		ArrayList<String> nombres = new ArrayList<String>();

		nombres.add("dani");
		nombres.add("teruel");
		nombres.add("marc");
		nombres.add("davilillo");
		nombres.add("motoMongo");
	
		assertEquals("Perdemos, Jaume ha fedeado como de costumbre",JaimeHierroIV.hierro4(nombres, 5));
	
	}
	
	@Test
	public void test4() {
		
		ArrayList<String> nombres = new ArrayList<String>();

		nombres.add("dani");
		nombres.add("teruel");
		nombres.add("davilillo");
		nombres.add("motoMongo");
	
		assertEquals("Mejor no jugamos partidas normales",JaimeHierroIV.hierro4(nombres, 4));
	
	}

	@Test
	public void test5() {
		
		ArrayList<String> nombres = new ArrayList<String>();
	
		assertEquals("Mejor no jugamos partidas normales",JaimeHierroIV.hierro4(nombres, 0));
	
	}
	
	@Test
	public void test6() {
		
		ArrayList<String> nombres = new ArrayList<String>();
	
		nombres.add("dani");
		nombres.add("raul");
		nombres.add("davilillo");
		nombres.add("motoMongo");
		nombres.add("ethan");
		nombres.add("ruben");
		nombres.add("ferrandiz");
		nombres.add("marc");
		nombres.add("rajoy es fuerte");
		
		assertEquals("Mejor no jugamos partidas normales",JaimeHierroIV.hierro4(nombres, 9));
	
	}
}
